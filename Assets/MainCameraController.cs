﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour
{
    public GameObject player;
    public float speedHorizontal;
    public float speedVertical;
    public float minVerticalAngle;
    public float maxVerticalAngle;

    private float x, y;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //camera rotation by mouse script
        x += Input.GetAxis("Mouse Y") * speedVertical * Time.deltaTime;

        if (x <= maxVerticalAngle && x >= minVerticalAngle)
        {
            //no instruction, all is fine
        }
        else if (x > maxVerticalAngle)
        {
            x = maxVerticalAngle;
        }
        else
        {
            x = minVerticalAngle;
        }

        y += Input.GetAxis("Mouse X") * speedHorizontal * Time.deltaTime;

        transform.rotation = Quaternion.Euler(x, y, 0);
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}
